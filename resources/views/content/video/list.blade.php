
@extends('layouts/contentLayoutMaster')

@section('title', 'Videos')

@section('content')
<style>
  .searchbtn{
    min-width: 135%;
    max-width: 100%;
    padding: 10px !important;
  }
</style>
    



<div class = "row">
  
  <div class = "col-md-10"></div>
  <div class="col-md-2 text-right">
    <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-7">
          <a href="{{route('add_video')}}" class="btn btn-primary searchbtn"><i data-feather="plus"></i>
            Add</a>
      </div>
    </div>
    
  </div>
</div>
<br/>
  <div class = "row">
      <div class = "col-md-10">
        <form action="{{route('video_list')}}" method ="get">
        <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
      </div>
      <div class="col-md-2 text-right">
        <div class="row">
          <div class="col-md-6">
            <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
          </div>
          <div class="col-md-6">
              <a href="{{route('video_list')}}" class="btn btn-primary searchbtn">Clear</a>
          </div>
        </div>
        </form>
      </div>
  </div>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Video</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @if(count($videos) == 0)
            <tr>
              <td colspan="2" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($videos as $video) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$video->title}}</span>
              </td>
              <td>
                  <span class="font-weight-bold"><a href="{{$video->url}}" target="_blank">{{$video->url}}</a></span>
              </td>
              
              <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('edit_video_view',[$video->id])}}" title="Edit">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="dropdown-item" href="{{route('delete_video',[$video->id])}}" onclick="return confirm('Are you sure you want to delete this Video?');" title="Delete">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $videos->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
