
@extends('layouts/contentLayoutMaster')

@section('title', 'Videos')

@section('content')
<form action="{{route('add_video')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  
  <div class="row form-group">
      <label>Title:</label>
      <input class="form-control" type="text" name="title" id="title">
        @error('title')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
            </span>
        @enderror
  </div>

  <div class="row form-group">
      <label>Video link:</label>
      <input class="form-control" type="text" name="video" id="file">
      <p>{{$message}}</p>
        @error('video')
            <span class="help-block" role="alert">
                <strong>{{$errors->first('video')}}</strong>
            </span>
        @enderror
  </div>
  
  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
