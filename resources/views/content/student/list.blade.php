
@extends('layouts/contentLayoutMaster')

@section('title', 'Students')

@section('content')
<style>
  .searchbtn{
    width: 135% !important;
    padding: 10px !important;
  }
</style>
  
<div class = "row">
    <div class = "col-md-10">
      <form action="{{route('student_list')}}" method ="get">
      <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
    </div>
    <div class="col-md-2 text-right">
      <div class="row">
        <div class="col-md-6">
          <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
        </div>
        <div class="col-md-6">
            <a href="{{route('student_list')}}" class="btn btn-primary searchbtn">Clear</a>
        </div>
      </div>
      </form>
    </div>
</div>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>School Name</th>
                <th>Gender</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count($students) == 0)
            <tr>
              <td colspan="2" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($students as $student) 
            <tr>
                <td>
                  <span class="font-weight-bold">{{$student->name}}</span>
                </td>
                <td>
                  <span class="font-weight-bold">{{$student->full_name}}</span>
                </td>
                <td>
                  <span class="font-weight-bold">{{$student->email}}</span>
                </td>
                <td>
                  <span class="font-weight-bold">{{$student->school->name}}</span>
                </td>
                <td>
                  <span class="font-weight-bold">{{$student->gender}}</span>
                </td>
                <td>
                  <span class="font-weight-bold">
                      @if($student->status == 0) <span class="m-badge  m-badge--primary m-badge--wide">Pending</span> @elseif($student->status == 1) <span class="m-badge  m-badge--success m-badge--wide">Active</span> @else <span class="m-badge  m-badge--danger m-badge--wide">InActive</span> @endif
                  </span>
                </td>
                  
                <td>
                  <div class="dropdown">
                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                      <i data-feather="more-vertical"></i>
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{route('edit_student_view',$student->id)}}">
                        <i data-feather="edit-2" class="mr-50"></i>
                        <span>Edit</span>
                      </a>
                      {{-- <a class="dropdown-item" href="javascript:void(0);">
                        <i data-feather="trash" class="mr-50"></i>
                        <span>Delete</span>
                      </a> --}}
                    </div>
                  </div>
                </td>
            </tr>
            @endforeach
           
          </tbody>
        </table>
        {{--  {{ $students->links('paginator.default') }}  --}}
        {{ $students->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
