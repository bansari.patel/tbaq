
@extends('layouts/contentLayoutMaster')

@section('title', 'Students')

@section('content')
<form action="{{route('edit_student')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  <input type="hidden" value="{{$edit->id}}" name="id">
    <div class="row form-group">
        <label>First Name:</label>
        <input class="form-control" type="text" name="name" id="name" value="{{$edit->name}}">
            @error('name')
                <span class="help-block" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
                </span>
            @enderror
    </div>

    <div class="row form-group">
        <label>Last Name:</label>
        <input class="form-control" type="text" name="last_name" id="name" value="{{$edit->full_name}}">
        @error('last_name')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Email:</label>
        <input class="form-control" type="email" name="email" id="email" value="{{$edit->email}}" readonly>
        @error('email')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>School Name:</label>
        <select class="form-control" name="school_id" id="school_id">
            @foreach($schools as $school)
                <option value="{{$school->id}}" @if($edit->school_id == $school->id) selected @endif>{{$school->name}}</option>
            @endforeach
        </select>
        @error('school_id')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('school_id') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Gender:</label>
        <select class="form-control" name="gender" id="gender">
            <option value="male" @if($edit->gender == 'male') selected @endif>Male</optiop>
            <option value="female" @if($edit->gender == 'female') selected @endif>Female</optiop>
        </select>
            
        @error('gender')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @enderror
    </div>

    <div class="row form-group">
        <label>School Year:</label>
        <input class="form-control" type="text" name="school_year" id="school_year" value="{{$edit->school_year}}">
        @error('school_year')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('school_year') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Lengue Bowler</label>&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="radio" name="league_bowler" id="league_bowler" value="yes" @if($edit->lengue_bowler == 'yes') checked @endif> Yes</input>&nbsp;&nbsp;
        <input type="radio" name="league_bowler" id="league_bowler" value="no" @if($edit->lengue_bowler == 'no') checked @endif> No</input>
        
        @error('league_bowler')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('league_bowler') }}</strong>
            </span>
        @enderror
    </div>

    <div class="row form-group">
        <label>Status:</label>
        <select class="form-control" name="status" id="status">
            <option value="1" @if($edit->status == '1') selected @endif>Active</optiop>
            <option value="2" @if($edit->status == '2') selected @endif>InActive</optiop>
        </select>
            
        @error('status')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('status') }}</strong>
            </span>
        @enderror
    </div>
  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
