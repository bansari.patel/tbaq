
@extends('layouts/contentLayoutMaster')

@section('title', 'Roll Strikes')

@section('content')
<style>
    .searchbtn{
      width: 135% !important;
      padding: 10px !important;
    }
  </style>


<div class = "row">
  
  <div class = "col-md-10"></div>
  <div class="col-md-2 text-right">
    <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-7">
          <a href="{{route('add_strike')}}" class="btn btn-primary searchbtn"><i data-feather="plus"></i>
            Add</a>
      </div>
    </div>
    
  </div>
</div>
<br/>


  
  <div class = "row">
      <div class = "col-md-10">
        <form action="{{route('roll_strike_list')}}" method ="get">
        <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
      </div>
      <div class="col-md-2 text-right">
        <div class="row">
          <div class="col-md-6">
            <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
          </div>
          <div class="col-md-6">
              <a href="{{route('roll_strike_list')}}" class="btn btn-primary searchbtn">Clear</a>
          </div>
        </div>
        </form>
      </div>
  </div>
  
<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>pdf</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @if(count($strikes) == 0)
            <tr>
              <td colspan="2" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($strikes as $strike) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$strike->title}}</span>
              </td>
              <td>
                
                <embed src="{{URL::asset('/strike/'.$strike->url)}}" width="50%" height="50%" />
              </td>
              
              <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('edit_strike_view',[$strike->id])}}" title="Edit">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="dropdown-item" href="{{route('delete_strike',[$strike->id])}}" onclick="return confirm('Are you sure you want to delete this Roll Strike?');"  title="Delete">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $strikes->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
