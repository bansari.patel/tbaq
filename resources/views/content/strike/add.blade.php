
@extends('layouts/contentLayoutMaster')

@section('title', 'Roll Strike')

@section('content')
<form action="{{route('add_strike')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  
  <div class="row form-group">
      <label>Title:</label>
      <input class="form-control" type="text" name="title" id="title">
        @error('title')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
            </span>
        @enderror
  </div>

  <div class="row form-group">
      <label>Select Pdf to upload:</label>
      <input class="form-control" type="file" name="pdf" id="file">
        @error('pdf')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('pdf') }}</strong>
            </span>
        @enderror
  </div>
  
  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
