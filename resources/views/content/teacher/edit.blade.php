
@extends('layouts/contentLayoutMaster')

@section('title', 'Teachers')

@section('content')
<form action="{{route('edit_teacher')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  <input type="hidden" value="{{$edit->id}}" name="id">
    <div class="row form-group">
        <label>First Name:</label>
        <input class="form-control" type="text" name="name" id="name" value="{{$edit->name}}">
            @error('name')
                <span class="help-block" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
                </span>
            @enderror
    </div>

    <div class="row form-group">
        <label>Last Name:</label>
        <input class="form-control" type="text" name="last_name" id="name" value="{{$edit->full_name}}">
        @error('last_name')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Email:</label>
        <input class="form-control" type="email" name="email" id="email" value="{{$edit->email}}" readonly>
        @error('email')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>School Name:</label>
        <select class="form-control" name="school_id" id="school_id">
            @foreach($schools as $school)
                <option value="{{$school->id}}" @if($edit->school_id == $school->id) selected @endif>{{$school->name}}</option>
            @endforeach
        </select>
        @error('school_id')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('school_id') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Phone Number</label>
        <input class="form-control" type="number" name="phone_number" id="phone_number" value="{{$edit->phone_number}}">
        @error('phone_number')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
        @enderror
    </div>
    <div class="row form-group">
        <label>Status:</label>
        <select class="form-control" name="status" id="status">
            <option value="1" @if($edit->status == '1') selected @endif>Active</optiop>
            <option value="2" @if($edit->status == '2') selected @endif>InActive</optiop>
        </select>
            
        @error('status')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('status') }}</strong>
            </span>
        @enderror
    </div>
    
  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
