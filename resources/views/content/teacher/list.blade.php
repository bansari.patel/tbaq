
@extends('layouts/contentLayoutMaster')

@section('title', 'Teachers')

@section('content')
<style>
    .searchbtn{
      width: 135% !important;
      padding: 10px !important;
    }
  </style>
    
  <div class = "row">
      <div class = "col-md-10">
        <form action="{{route('teacher_list')}}" method ="get">
        <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
      </div>
      <div class="col-md-2 text-right">
        <div class="row">
          <div class="col-md-6">
            <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
          </div>
          <div class="col-md-6">
              <a href="{{route('teacher_list')}}" class="btn btn-primary searchbtn">Clear</a>
          </div>
        </div>
        </form>
      </div>
  </div>
  
<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>School Name</th>
              <th>Phone Number</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if(count($teachers) == 0)
            <tr>
              <td colspan="7" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($teachers as $teacher) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$teacher->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$teacher->full_name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$teacher->email}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$teacher->school->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$teacher->phone_number}}</span>
              </td>
              <td>
                <span class="font-weight-bold">
                    @if($teacher->status == 0) <span class="m-badge  m-badge--primary m-badge--wide">Pending</span> @elseif($teacher->status == 1) <span class="m-badge  m-badge--success m-badge--wide">Active</span> @else <span class="m-badge  m-badge--danger m-badge--wide">InActive</span> @endif
                </span>
              </td>
                
              <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('edit_teacher_view',$teacher->id)}}">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    {{-- <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a> --}}
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $teachers->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
