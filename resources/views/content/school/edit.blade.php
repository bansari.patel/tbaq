
@extends('layouts/contentLayoutMaster')

@section('title', 'Schools')

@section('content')
<form action="{{route('edit_school')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  <input type="hidden" value="{{$edit->id}}" name="id">
  <div class="row form-group">
      <label>Name:</label>
      <input class="form-control" type="text" name="name" id="name" value="{{$edit->name}}">
        @error('name')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
        @enderror
  </div>

    <div class="row form-group">
        <label>Address:</label>
        <input class="form-control" type="text" name="address" id="address" value="{{$edit->address}}">
        @error('address')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('address') }}</strong>
            </span>
        @enderror
    </div>

    <div class="row form-group">
        <label>Status:</label>
        <select class="form-control" name="status" id="status">
            <option value="1" @if($edit->status == 1) selected @endif>Active</option>
            <option value="2" @if($edit->status == 2) selected @endif>InActive</option>
        </select>
        @error('status')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('status') }}</strong>
            </span>
        @enderror
    </div>

  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
