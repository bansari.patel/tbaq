
@extends('layouts/contentLayoutMaster')

@section('title', 'Schools')

@section('content')
<style>
  .searchbtn{
    width: 135% !important;
    padding: 10px !important;
  }
</style>



<div class = "row">
  
  <div class = "col-md-10"></div>
  <div class="col-md-2 text-right">
    <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-7">
          <a href="{{route('add_school')}}" class="btn btn-primary searchbtn"><i data-feather="plus"></i>
            Add</a>
      </div>
    </div>
    
  </div>
</div>
<br/>
<div class = "row">
  
    <div class = "col-md-10">
      <form action="{{route('school_list')}}" method ="get">
      <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
    </div>
    <div class="col-md-2 text-right">
      <div class="row">
        <div class="col-md-6">
          <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
        </div>
        <div class="col-md-6">
            <a href="{{route('school_list')}}" class="btn btn-primary searchbtn">Clear</a>
        </div>
      </div>
      </form>
    </div>
</div>
<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @if(count($schools) == 0)
            <tr>
              <td colspan="3" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($schools as $school) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$school->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$school->address}}</span>
              </td>
              <td>
                <span class="font-weight-bold">
                  @if($school->status == 0) <span class="m-badge  m-badge--primary m-badge--wide">Pending</span> @elseif($school->status == 1) <span class="m-badge  m-badge--success m-badge--wide">Active</span> @else <span class="m-badge  m-badge--danger m-badge--wide">InActive</span> @endif
                </span>
              </td>
              <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('edit_school_view',[$school->id])}}" title="Edit">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    {{-- <a class="dropdown-item" href="{{route('delete_school',[$school->id])}}" onclick="return confirm('Are you sure you want to delete this School?');" title="Delete">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a> --}}
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $schools->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
