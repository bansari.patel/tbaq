
@extends('layouts/contentLayoutMaster')

@section('title', 'Schools')

@section('content')
<form action="{{route('add_school')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  
  <div class="row form-group">
      <label>Name:</label>
      <input class="form-control" type="text" name="name" id="name">
        @error('name')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
        @enderror
  </div>
  <div class="row form-group">
    <label>Address:</label>
    <input class="form-control" type="text" name="address" id="address">
      @error('address')
          <span class="help-block" role="alert">
          <strong>{{ $errors->first('address') }}</strong>
          </span>
      @enderror
</div>

  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>
  
</form>

@endsection
