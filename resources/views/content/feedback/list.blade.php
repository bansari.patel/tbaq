
@extends('layouts/contentLayoutMaster')

@section('title', 'Feedback')

@section('content')
<style>
  .searchbtn{
    width: 135% !important;
    padding: 10px !important;
  }
</style>
<div class = "row">
  
  <div class = "col-md-10">
    <form action="{{route('feedback_list')}}" method ="get">
    <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
  </div>
  <div class="col-md-2 text-right">
    <div class="row">
    <div class="col-md-6">
      <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
    </div>
    <div class="col-md-6">
        <a href="{{route('feedback_list')}}" class="btn btn-primary searchbtn">Clear</a>
    </div>
  </div>
  </form>
</div>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>School Name</th>
              <th style="width: 24%;">Total time for used kits</th>
              <th style="width: 21%;">Kits used by student</th>
              <th style="width: 20%;">Purchasing Kit</th>
              <th>Image</th>
              {{-- <th>Action</th> --}}
            </tr>
          </thead>
          <tbody>
            @if(count($feedbacks) == 0)
            <tr>
              <td colspan="7" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($feedbacks as $feedback) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$feedback->user->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$feedback->school->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$feedback->kits_used_time}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$feedback->kits_used_by_student}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$feedback->purchasing_kit}}</span>
              </td>
              <td>
                  @foreach($feedback->image as $image)
                  <img src="{{URL::asset('/feedback/'.$image->image)}}" style="width: 50%;margin-bottom:5%;"/><br/>
                  @endforeach
              </td>
              
              {{-- <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  </div>
                </div>
              </td> --}}
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $feedbacks->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
