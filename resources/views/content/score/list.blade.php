
@extends('layouts/contentLayoutMaster')

@section('title', 'Score')

@section('content')
<style>
  .searchbtn{
    width: 135% !important;
    padding: 10px !important;
  }
</style>

<div class = "row">
    <div class = "col-md-10">
      <form action="{{route('score_list')}}" method ="get">
      <input type="search" name="search"class="form-control" placeholder="Search" value="@if(!empty($search)) {{$search}} @endif"><br>
    </div>
    <div class="col-md-2 text-right">
      <div class="row">
        <div class="col-md-6">
          <button type = "submit" class ="btn btn-primary searchbtn">Search</button><br>
        </div>
        <div class="col-md-6">
            <a href="{{route('score_list')}}" class="btn btn-primary searchbtn">Clear</a>
        </div>
      </div>
      </form>
    </div>
</div>

<!-- Basic Tables start -->
<div class="row" id="basic-table">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>School Name</th>
              <th>Score</th>
              <th>Round</th>
              <th>Competition</th>
              <th style="width: 15%;">Image</th>
              {{-- <th>Action</th> --}}
            </tr>
          </thead>
          <tbody>
            @if(count($scores) == 0)
            <tr>
              <td colspan="7" style="text-align:center;">No data found</td>
            </tr>
            @endif
          @foreach($scores as $score) 
            <tr>
              <td>
                <span class="font-weight-bold">{{$score->user->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$score->school->name}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$score->score}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$score->round}}</span>
              </td>
              <td>
                <span class="font-weight-bold">{{$score->competition}}</span>
              </td>
              <td>
                  <img src="{{URL::asset('/score/'.$score->image)}}" style="width: 100%;">
              </td>
              
              {{-- <td>
                <div class="dropdown">
                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="edit-2" class="mr-50"></i>
                      <span>Edit</span>
                    </a>
                    <a class="dropdown-item" href="javascript:void(0);">
                      <i data-feather="trash" class="mr-50"></i>
                      <span>Delete</span>
                    </a>
                  </div>
                </div>
              </td> --}}
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $scores->links() }}
      </div>
    </div>
  </div>
</div>
<!-- Basic Tables end -->

@endsection
