
@extends('layouts/contentLayoutMaster')

@section('title', 'Locations')

@section('content')
<form action="{{route('add_location')}}" method="post" enctype="multipart/form-data" id="form1">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">

  <div class="row form-group">
      <label>Name:</label>
      <input class="form-control" type="text" name="address" id="address">
        @error('address')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('address') }}</strong>
            </span>
        @enderror
  </div>

  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>

</form>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
              $("#form1").validate({
                rules: {
                    address: {
                        required: true,
                    }

                 },
                messages: {
                    }
                  });
                });

        </script>

@endsection
