
@extends('layouts/contentLayoutMaster')

@section('title', 'Locations')

@section('content')
<form action="{{route('edit_location')}}" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{ csrf_token() }}" name="_token">
  <input type="hidden" value="{{$edit->id}}" name="id">
  <div class="row form-group">
      <label>Name:</label>
      <input class="form-control" type="text" name="address" id="address" value="{{$edit->address}}">
        @error('address')
            <span class="help-block" role="alert">
            <strong>{{ $errors->first('address') }}</strong>
            </span>
        @enderror
  </div>

  <div class="row form-group">
      <input class="form-control btn-primary" type="submit" value="Submit" name="Submit" style="width:10%;">
  </div>

</form>

@endsection
