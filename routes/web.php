<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});
Auth::routes();


Route::group(['middleware' => 'auth'], function(){
Route::get('/', 'HomeController@index')->name('home');

Route::get('/location', 'LocationController@index')->name('location_list');
Route::get('/add/location', 'LocationController@add_view')->name('add_location_view');
Route::post('/add/location', 'LocationController@add')->name('add_location');
Route::get('/edit/location/{id}', 'LocationController@edit_view')->name('edit_location_view');
Route::post('/edit/location', 'LocationController@edit')->name('edit_location');
Route::get('/delete/location/{id}', 'LocationController@delete')->name('delete_location');

Route::get('/school', 'SchoolController@index')->name('school_list');
Route::get('/add/school', 'SchoolController@add_view')->name('add_school_view');
Route::post('/add/school', 'SchoolController@add')->name('add_school');
Route::get('/edit/school/{id}', 'SchoolController@edit_view')->name('edit_school_view');
Route::post('/edit/school', 'SchoolController@edit')->name('edit_school');
Route::get('/delete/school/{id}', 'SchoolController@delete')->name('delete_school');


Route::get('/student','StudentController@index')->name('student_list');
Route::get('/edit/student/{id}', 'StudentController@edit_view')->name('edit_student_view');
Route::post('/edit/student', 'StudentController@edit')->name('edit_student');

Route::get('/teacher','TeacherController@index')->name('teacher_list');
Route::get('/edit/teacher/{id}', 'TeacherController@edit_view')->name('edit_teacher_view');
Route::post('/edit/teacher', 'TeacherController@edit')->name('edit_teacher');

Route::get('/videos', 'VideoController@index')->name('video_list');
Route::get('/add/video', 'VideoController@add_view')->name('add_video_view');
Route::post('/add/video', 'VideoController@add')->name('add_video');
Route::get('/edit/video/{id}', 'VideoController@edit_view')->name('edit_video_view');
Route::post('/edit/video', 'VideoController@edit')->name('edit_video');
Route::get('/delete/video/{id}', 'VideoController@delete')->name('delete_video');

Route::get('/roll/strikes', 'RollStrikeController@index')->name('roll_strike_list');
Route::get('/add/strike', 'RollStrikeController@add_view')->name('add_strike_view');
Route::post('/add/strike', 'RollStrikeController@add')->name('add_strike');
Route::get('/edit/strike/{id}', 'RollStrikeController@edit_view')->name('edit_strike_view');
Route::post('/edit/strike', 'RollStrikeController@edit')->name('edit_strike');
Route::get('/delete/{id}','RollStrikeController@delete')->name('delete_strike');

Route::get('/score/list','ScoreController@index')->name('score_list');

Route::get('/feedback/list','FeedbackController@index')->name('feedback_list');
});