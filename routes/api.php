<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', 'API\StudentController@register');
Route::post('/login','API\StudentController@login');
Route::post('/password/email', 'API\StudentController@forgot');
Route::view('/forgot_password', 'auth.passwords.reset')->name('password.reset');
Route::post('/password/reset', 'API\StudentController@reset');

Route::get('/school','API\StudentController@school');

Route::group(['middleware' => 'auth:api'], function(){
Route::get('/location','API\StudentController@location');
Route::post('/add/score','API\StudentController@add_score');
Route::get('/video/list','API\StudentController@video_list');
Route::get('/profile/view/{id}','API\StudentController@profile_view');
Route::post('/edit/profile','API\StudentController@edit_profile');
Route::post('/add/score','API\StudentController@add_score');
Route::get('/setting','API\StudentController@setting');
Route::get('/roll/strike/list','API\TeacherController@roll_strike_list');
Route::post('/add/feedback','API\TeacherController@add_feedback');
Route::get('/score/list','API\TeacherController@score_list');
Route::post('/change/password','API\StudentController@changePassword');
});

