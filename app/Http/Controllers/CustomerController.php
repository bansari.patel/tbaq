<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Customers"]
        ];
        $customers = Customer::get();
        return view('/content/customers/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'customers' => $customers]);
    }
}
