<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Location;
use App\School;
use App\Video;
use App\Score;
use App\Setting;
use Mail;
use Illuminate\Support\Facades\Password;
class StudentController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();

        if($data['role'] == 'student'){
            $validator = Validator::make($request->all(),[
                'first_name'=>'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'role' => 'required',
                'school_id' => 'required',
                'league_bowler' => 'required',
                'school_year' =>'required',
            ]);
        }
        if($data['role'] == 'teacher'){
            $validator = Validator::make($request->all(),[
                'first_name'=>'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'role' => 'required',
                'school_id' => 'required',
                'phone_number' => 'required',
            ]);
        }

        if($validator->fails()){
            $message = "Something Wrong Please try again";
            if($validator->errors()->first()){
                $message = $validator->errors()->first();
            }
            return response()->json(['status'=>false,'statusCode' => '400' ,'message' =>$message,'data' => $validator->errors()]);
        }else{

            $school = School::where('name',$data['school_id'])->get();
            if(count($school) == 0){
                $school = new School();
                $school->name = $data['school_id'];
                $school->save();
                $school_id = $school->id;
            }
            else{
                $school_id = $school[0]->id;

            }
            $user = new User();
            $user->name = $data['first_name'];
            $user->full_name = $data['last_name'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->role = $data['role'];
            $user->school_id = $school_id;


            if($data['role'] == 'student'){
                if(isset($data['dob'])){
                    $user->dob =  $data['dob'];
                }
                if(isset($data['gender'])){
                    $user->gender = $data['gender'];
                }
                
                $user->lengue_bowler = $data['league_bowler'];
                $user->school_year = $data['school_year'];
                $user->save();
            }

            if($data['role'] == 'teacher'){

                $user->phone_number = $data['phone_number'];
                $user->save();
            }

            if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']] ))
            {

                $users = Auth::user();
                $users->first_name = $users->name;
                $users->last_name = $users->full_name;
                $users->school_name = $users->school->name;
                $success['user'] = $users;
                $success['token'] =  $users->createToken('MyApp')->accessToken;

            }
            return response()->json(['status'=>true,'statusCode' => '200' ,'message'=>'User Register Successfully','data'=>$success]);
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $message = "Something Wrong Please try again";
            if($validator->errors()->first()){
                $message = $validator->errors()->first();
            }
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" => $message,'data' => $validator->errors()]);
        }
        $user = User::where('email',$request->get('email'))->where('status','1')->get();
        if(count(($user)) > 0)
        {
            if(Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')] ))
            {

                    $user = Auth::user();
                    $user->first_name = $user->name;
                    $user->last_name = $user->full_name;

                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                    $user->school_name = $user->school->name;
                    $success['user'] = $user;
                    $message =  "Login successfuly";
                    return response()->json(['status'=> true,'statusCode' => '200','message ' => $message,'data' =>  $success]);
            }
            else{
                return response()->json(['status'=> false,'statusCode'=> '400',"message" => "Invalid username or password.","data"=> null]);
            }
        }
        else{
            return response()->json(['status'=> false,'statusCode'=> '400',"message" => "Your Account is Inactive. Please contact to Administrator","data"=> null]);
        }
    }

    public function forgot(Request $request) {
        $data = $request->all();
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

        return response()->json(['status'=> true, 'statusCode' =>'200' , 'message' => 'Reset password link sent on your email id.' ,'data' =>$data['email']]);
    }

    public function reset(Request $request) {
        $data = $request->all();


        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(['status'=> false, 'statusCode' =>'400' , 'message' => 'Invalid token provided.' ,'data' =>$data]);
        }

        return response()->json(['status'=> true, 'statusCode' =>'200' , 'message' => 'Password has been successfully changed.' ,'data' =>$data]);
    }

    public function changePassword(Request $request) {
        $data = $request->all();
        $id = Auth::user()->id;
        $user = User::find($id);

        $credentials = request()->validate([
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        if (Hash::check($data['old_password'], $user->password)) {
            $profile = User::find($id);
            $profile->password = Hash::make($data['new_password']);
            $profile->save();

            return response()->json(['status'=> true, 'statusCode' =>'200' , 'message' => 'Password has been successfully changed.' ,'data' =>$profile]);
        }
        else{
            return response()->json(['status'=> false, 'statusCode' =>'400' , 'message' => 'Please check Old password' ,'data' =>$data]);
        }
    }
    public function school(Request $request){

        $school = School::get();

        if(count($school) >0){
            return response()->json(['status'=> true, 'statusCode' =>'200' , 'message' => 'School List' ,'data' =>$school]);
        }else{
            return response()->json(['status' =>false,'statusCode' =>'400' ,'message' => 'No data found','data'=> null]);
        }
    }

    public function location(Request $request){

        $location = Location::get();

        if(count($location) >0){
            return response()->json(['status'=> true, 'statusCode' =>'200' , 'message' => 'Location List' ,'data' =>$location]);
        }else{
            return response()->json(['status' =>false,'statusCode' =>'400' ,'message' => 'No data found','data'=> null]);
        }
    }

    public function add_score(Request $request){
        $data = $request->all();

        $user = Auth::user();
        $validator = Validator::make($request->all(),[
            'score' => 'required',
            'competition' => 'required',
            'interschool_challenge' => 'required',
            'image' => 'required',
            'location_id' =>'required',
        ]);

        if($validator->fails()){

            return response()->json(['status'=>false,'statusCode' =>'400','message' => 'Something Wrong please try again...','data' =>null]);
        }else{

            if($request->file('image') != ''){
                $image = $request->file('image');
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/score');
                $image->move($destinationPath, $imageName);
            }

            $add = new Score();
            $add->user_id = $user->id;
            $add->school_id = $user->school_id;
            $add->location_id = $data['location_id'];
            $add->score = $data['score'];
            $add->competition = $data['competition'];
            $add->interschool_challenge = $data['interschool_challenge'];
            $add->image = $imageName;
            $add->save();

            // if($add->id != ''){

            //     $sdata = Score::with('school','user')->where('id' ,$add->id)->get()->first();
            //     $template_data = [  'name' => $sdata->user->name,
            //                         'email' => $sdata->user->email,
            //                         'school' => $sdata->school->name,
            //                         'score' => $sdata->score,
            //                         'round' => $sdata->round,
            //                         'competition' =>$sdata->competition,
            //                         'interschool_challenge' => $sdata->interschool_challenge,
            //                         'gender' => $sdata->user->gender,
            //                         'lengue_bowler' => $sdata->user->lengue_bowler];

            //     //send verification code
            //     $email = $user->email;
            //     Mail::send(['html' => 'email.score'], $template_data,
            //                 function ($message) use ($email) {
            //                     $message->to('dev@tbaq.org.au')
            //                     ->from($email) //not sure why I have to add this
            //                     ->subject('TBAQ Score');
            //     });
            // }


            return response()->json(['status'=>true,'statusCode' =>'200','message' => 'Added to Score','data'=> $data]);

        }
    }

    public function video_list(Request $request){

        $video = Video::get();

        if(count($video)){

            return response()->json(['status' => true,'statusCode' => '200','message' => 'Video List' ,'data' =>$video]);
        }else{

            return response()->json(['status' => true,'statusCode' => '400','message' =>'No data found','data'=>null]);
        }
    }

    public function profile_view(Request $request, $id){
        $users = Auth::user();
        $data = [];
        $profile = User::with('school')->where('id',$users->id)->get()->first();
        $profile->first_name = $profile ->name;
        $profile->last_name = $profile ->full_name;
        $data['user'] = $profile;
        return response()->json(['status' => true ,'statusCode' => '200' ,'message' => 'Profile info..','data' => $data]);
    }

    public function edit_profile(Request $request){
        $userInfo = Auth::user();
        $data = $request->all();
        $user = User::find($userInfo->id);

        if($user->role == 'student'){
            $validator = Validator::make($request->all(),[
                'first_name'=>'required',
                'last_name' => 'required',
                'school_id' => 'required',
                'league_bowler' => 'required',
                'school_year' =>'required',

            ]);
        }
        if($user->role == 'teacher'){
            $validator = Validator::make($request->all(),[
                'first_name'=>'required',
                'last_name' => 'required',
                'school_id' => 'required',
                'phone_number' => 'required',

            ]);
        }
        if($validator->fails()){
            return response()->json(['status'=> false,'statusCode' => '400', 'message' =>'Something Wrong Please try again' ,'data' =>$validator->errors()]);
        }
        $school = School::where('name',$data['school_id'])->get();
            if(count($school) == 0){
                $school = new School();
                $school->name = $data['school_id'];
                $school->save();
                $school_id = $school->id;
            }
            else{
                $school_id = $school[0]->id;

        }
        $edit = User::find($userInfo->id);
        $edit->name = $data['first_name'];
        $edit->full_name = $data['last_name'];
        $edit->school_id = $school_id;

        if($user->role == 'student'){
            if(isset($data['dob'])){
                $edit->dob =  $data['dob'];
            }
            if(isset($data['gender'])){
                $edit->gender = $data['gender'];
            }
            
            $edit->lengue_bowler = $data['league_bowler'];
            $edit->school_year = $data['school_year'];
            $edit->save();
        }

        if($user->role == 'teacher'){

            $edit->phone_number = $data['phone_number'];
            $edit->save();
        }

        $profile = User::with('school')->where('id',$userInfo->id)->get()->first();
        $profile->first_name = $profile ->name;
        $profile->last_name = $profile ->full_name;
        $data['user'] = $profile;
        return response()->json(['status' => true ,'statusCode' => '200' , 'message' => 'Profile Edited Successfully' , 'data' =>$data]);

    }

    public function setting(Request $request){

        $setting = Setting::get();
        $url = "http://www.tbaq.org.au/";


        foreach($setting as &$set){


            $shopping_url_path = $url.$set->shopping_url;
            $set->shopping_url_path =$shopping_url_path;

            $booking_url_path = $url.$set->booking_url;
            $set->booking_url_path = $booking_url_path;

        }
        return response()->json(['status' => true ,'statusCode' => '200' ,'message' => 'Setting info..','data' => $setting]);
    }

}
