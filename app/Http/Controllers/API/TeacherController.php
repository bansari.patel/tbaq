<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Feedback;
use App\FeedbackImage;
use App\RollStrike;
use Mail;
use App\Score;
class TeacherController extends Controller
{
    public function roll_strike_list(Request $request){
        
        $list = RollStrike::get();

        if(count($list)){
            foreach($list as &$l){

        
                $path = url('/strike/'.$l->url);
                $l->pdf_path =$path;
            }    
            return response()->json(['status' => true,'statusCode' => '200','message' => 'Roll Strike list...' ,'data' =>$list]);
        }else{

            return response()->json(['status' => true,'statusCode' => '400','message' =>'No data found','data'=>null]);
        }
    }

    public function add_feedback(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($request->all(),[
            'kits_used_time' => 'required',
            'kits_used_by_student' => 'required',
            'feedback_option' => 'required',
            'purchasing_kit' =>'required',
           
        ]);

        
        if($validator->fails()){
             
            return response()->json(['status'=>false,'statusCode' =>'400','message' => 'Something Wrong please try again...','data' =>null]);
        }else{
            $i= 0;
            $add = new Feedback();
            $add->user_id = $user->id;
            $add->school_id = $user->school_id;
            $add->kits_used_time = $data['kits_used_time'];
            $add->kits_used_by_student = $data['kits_used_by_student'];
            $add->feedback_option = $data['feedback_option'];
            $add->purchasing_kit = $data['purchasing_kit'];
            $add->recommend_for_other_school = $data['recommend_for_other_school'];
            $add->save();
            
            if($add->id != '')
            {
                if(isset($data['image'])){

                
                foreach($data['image'] as $image){
                
                $imageName = time().$i.'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/feedback');
                $image->move($destinationPath, $imageName);
                $i++;

                $feedImage = new FeedbackImage();
                $feedImage->feedback_id = $add->id;
                $feedImage->image = $imageName;
                $feedImage->save();
               
                }
                }
            }
            // if($add->id != ''){

            //     $fdata = Feedback::with('school','user')->where('id' ,$add->id)->get()->first();
            //     $template_data = [  'name' => $fdata->user->name,
            //                         'email' => $fdata->user->email,
            //                         'school' => $fdata->school->name,
            //                         'kits_used_time' => $fdata->kits_used_time,
            //                         'kits_used_by_student' => $fdata->kits_used_by_student,
            //                         'kits_photo_issue' =>$fdata->kits_photo_issue,
            //                         'feedback_option' => $fdata->feedback_option,
            //                         'purchasing_kit' => $fdata->purchasing_kit];
            //     //send verification code
            //     $email = $user->email;
            //     Mail::send(['html' => 'email.feedback'], $template_data,
            //                 function ($message) use ($email) {
            //                     $message->to('dev@tbaq.org.au')
            //                     ->from($email) //not sure why I have to add this
            //                     ->subject('TBAQ Teachers Feedback');
            //     });
            // }
            return response()->json(['status'=>true,'statusCode' =>'200','message' => 'Feedback Added Successfully','data'=> $data]);

        }
    }

    public function score_list(Request $request){
        $user = Auth::user();
        $score = Score::with('location')->where('user_id',$user->id)->orderBy('id', 'desc')->paginate('20');

        return response()->json(['status'=>true,'statusCode' =>'200','message' => 'Score List','data'=> $score]);

    }
    

    
    
}
