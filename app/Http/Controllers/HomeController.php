<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Home"]
        ];
        return view('/content/home');
    }
}
