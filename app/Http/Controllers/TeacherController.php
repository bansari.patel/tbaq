<?php

namespace App\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use App\User;
use App\School;
class TeacherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Teachers"]
        ];

        if($search != ''){
            $teachers = User::with('school')->where('role','teacher')
                                            ->where('name','like',"%$search%")
                                            ->orWhere('email','like',"%$search%")
                                            ->orWhere('full_name','like',"%$search%")
                                            ->paginate('20');
            $search = $search;
        }else{
            $teachers = User::with('school')->where('role','teacher')->paginate('20');
            $search = "";
        }
        
        return view('/content/teacher/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'teachers' => $teachers , 'search' => $search]);
    }
    public function edit_view(Request $request,$id){

        $schools = School::get();
        $user = User::find($id);
        return view('/content/teacher/edit', [ 'schools' => $schools ,'edit' => $user]);
    }

    public function edit(Request $request){
        $data = $request->all();

        $validator = Validator::make($request->all(),[
                'name'=>'required',
                'last_name' => 'required',
                'school_id' => 'required',
                'phone_number' =>'required',
                'status' => 'required',
            ]);
        
        
            if($validator->fails() ){
                return redirect(route('edit_teacher_view',$data['id']))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            
            $user = User::find($data['id']);
            $user->name = $data['name'];
            $user->full_name = $data['last_name'];
            $user->school_id = $data['school_id'];
            $user->email = $data['email'];
            $user->phone_number = $data['phone_number'];
            $user->status = $data['status'];
            $user->save();
            
            return redirect('/teacher');
    

    }
}
