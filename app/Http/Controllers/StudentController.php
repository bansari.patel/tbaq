<?php

namespace App\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use App\User;
use App\School;
class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Students"]
        ];

        if($search != ''){
            $students = User::with('school')->where('role','student')
                            ->where('name','like',"%$search%")
                            ->orWhere('email','like',"%$search%")
                            ->orWhere('full_name','like',"%$search%")
                            ->paginate('20');

                            
            $search = $search;
        }else{
            $students = User::with('school')->where('role','student')->paginate('20');
            $search = "";
        }
        
        return view('/content/student/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'students' => $students, 'search' => $search]);
    }

    public function edit_view(Request $request,$id){

        $schools = School::get();
        $user = User::find($id);
        return view('/content/student/edit', [ 'schools' => $schools ,'edit' => $user]);
    }

    public function edit(Request $request){
        $data = $request->all();

        $validator = Validator::make($request->all(),[
                'name'=>'required',
                'last_name' => 'required',
                'school_id' => 'required',
                'gender' =>'required',
                'league_bowler' => 'required',
                'school_year' =>'required',
                'status' =>'required',
            ]);
        
        
            if($validator->fails() ){
                return redirect(route('edit_student_view',$data['id']))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            
            $user = User::find($data['id']);
            $user->name = $data['name'];
            $user->full_name = $data['last_name'];
            $user->school_id = $data['school_id'];
            $user->email = $data['email'];
            $user->gender = $data['gender'];
            $user->lengue_bowler = $data['league_bowler'];
            $user->school_year = $data['school_year'];
            $user->status = $data['status'];
            $user->save();
            
            return redirect('/student');
    

    }
}
