<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use Validator;
class FeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Feedback"]
        ];

        if($search != ''){
            $feedback = Feedback::with('user','school','image')
                                    ->whereHas('user', function ($query) use ($search) 
                                        { $query->where('users.name','like',"%$search%");
                                        })
                                    ->whereHas('school', function ($query) use ($search) 
                                        { $query->where('schools.name','like',"%$search%");
                                        })
                                ->orWhere('kits_used_time','like',"%{$search}%")
                                ->orWhere('kits_used_by_student','like',"%{$search}%")
                                ->orWhere('kits_photo_issue','like',"%{$search}%")
                                ->orWhere('feedback_option','like',"%{$search}%")
                                ->orWhere('purchasing_kit','like',"%{$search}%")
                                ->paginate('20');
            $search = $search;
        }else{
            $feedback = Feedback::with('user','school','image')->paginate('20');
            $search = "";
        }
        
        return view('/content/feedback/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'feedbacks' => $feedback, 'search' => $search]);
    }

    
}
