<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RollStrike;
use Validator;
class RollStrikeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Roll Strikes"]
        ];

        if($search != ''){
            $strikes = RollStrike::where('title','like',"%$search%")->paginate('20');
            $search = $search;
        }else{
            $strikes = RollStrike::paginate('20');
            $search = "";
        }
        
        return view('/content/strike/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'strikes' => $strikes,'search' => $search]);
    }

    public function add_view(Request $request)
    {
        return view('/content/strike/add');
    }

    public function add(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'pdf' => "required|mimetypes:application/pdf|max:10000"


        ]);

        if($validator->fails() ){
            return redirect(route('add_strike_view'))
                        ->withErrors($validator)
                        ->withInput();
        }

        if($request->file('pdf') != ''){
            $strike = $request->file('pdf');
            $strikeName = time().'.'.$strike->getClientOriginalExtension();
            $destinationPath = public_path('/strike');
            $strike->move($destinationPath, $strikeName);
        }

        $add = new RollStrike();
        $add->title = $data['title'];
        $add->url = $strikeName;
        $add->save();

        return redirect('/roll/strikes');
    }

    public function edit_view(Request $request,$id){
        $strike = RollStrike::find($id);
        return view('/content/strike/edit')->with('edit',$strike);
    }

    public function edit(Request $request){

        $data = $request->all();
        $validator = Validator::make($request->all(),[
            'title' => 'required',
        ]);

        if($validator->fails()){

            return redirect(route('edit_strike_view',[$data['id']]))
            ->withErrors($validator)
            ->withInput();
        }

        $strike = RollStrike::find($data['id']);
        if($request->file('pdf') != ''){
            $strike = $request->file('pdf');
            $strikeName = time().'.'.$strike->getClientOriginalExtension();
            $destinationPath = public_path('/strike');
            $strike->move($destinationPath, $strikeName);
        }else{
            $strikeName = $strike->url;
        }

        $edit = RollStrike::find($data['id']);
        $edit->title = $data['title'];
        $edit->url = $strikeName;
        $edit->save();

        return redirect('/roll/strikes');
    }

    public function delete(Request $request, $id){

        RollStrike::where('id',$id)->delete();

        return redirect('/roll/strikes');
    }
}
