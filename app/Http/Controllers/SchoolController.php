<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\School;
class SchoolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"School"]
        ];

        if($search != ''){
            $schools = School::where('name','like',"%$search%")->orWhere('address','like',"%$search%")->paginate('20');
            $search = $search;
        }else{
            $schools = School::paginate('20');
            $search = "";
        }
        
        return view('/content/school/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'schools' => $schools,'search' => $search]);
    }

    public function add_view(Request $request)
    {
        return view('/content/school/add');
    }

    public function add(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'address' => 'required',
        ]);

        if($validator->fails() ){
            return redirect(route('add_school_view'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $add = new School();
        $add->name = $data['name'];
        $add->address = $data['address'];
        $add->save();

        return redirect('/school');
    }

    public function edit_view(Request $request,$id)
    {
        $school = School::find($id);
        return view('/content/school/edit')->with('edit',$school);
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'address' => 'required',
            'status' =>'required'
        ]);

        if($validator->fails() ){
            return redirect(route('edit_school_view',[$data['id']]))
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $edit = School::find($data['id']);
        $edit->name = $data['name'];
        $edit->address = $data['address'];
        $edit->status = $data['status'];
        $edit->save();

        return redirect('/school');
    }

    public function delete(Request $request, $id){

        School::where('id',$id)->delete();

        return redirect('/school');
    }



}
