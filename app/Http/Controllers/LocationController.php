<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Location;
class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Location"]
        ];

        if($search != ''){
            $locations= Location::where('address','like',"%{$search}%")->paginate('20');
            $search = $search;
        }else{
            $locations = Location::paginate('20');
            $search = '';
        }
        return view('/content/location/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'locations' => $locations ,'search' =>$search]);
    }

    public function add_view(Request $request)
    {
        return view('/content/location/add');
    }

    public function add(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'address' => 'required'
        ]);

        if($validator->fails() ){
            return redirect(route('add_location_view'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $add = new Location();
        $add->address = $data['address'];
        $add->save();

        return redirect('/location');
    }

    public function edit_view(Request $request,$id)
    {
        $location = Location::find($id);
        return view('/content/location/edit')->with('edit',$location);
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'address' => 'required',
        ]);

        if($validator->fails() ){
            return redirect(route('edit_location_view',[$data['id']]))
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $edit = Location::find($data['id']);
        $edit->address = $data['address'];
        $edit->save();

        return redirect('/location');
    }

    public function delete(Request $request, $id){

        Location::where('id',$id)->delete();

        return redirect('/location');
    }



}
