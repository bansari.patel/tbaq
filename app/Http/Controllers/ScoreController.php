<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Score;
use Validator;
class ScoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search  = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Score"]
        ];

        if($search != ''){
            $score = Score::with('user','school')
                            ->where('score','like',"%$search%")
                            ->orWhere('round','like',"%$search%")
                            ->orWhere('competition','like',"%$search%")
                            ->orWhere('interschool_challenge','like',"%$search%")
                            ->paginate('20');
            $search = $search;
        }else{
            $score = Score::with('user','school')->paginate('20');
            $search = "";
        }
        
        return view('/content/score/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'scores' => $score, 'search' => $search]);
    }

    
}
