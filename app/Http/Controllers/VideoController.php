<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Video;
class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = $request->get('search');
        $pageConfigs = [];

        $breadcrumbs = [
            ['link'=>"home",'name'=>"Home"],['name'=>"Videos"]
        ];

        if($search != ''){
            $videos = Video::where('title','like',"%$search%")->paginate('20');
            $search = $search;
        }else{
            $videos = Video::paginate('20');
            $search = "";
        }
        
        return view('/content/video/list', [ 'pageConfigs' => $pageConfigs,'breadcrumbs' => $breadcrumbs , 'videos' => $videos, 'search' => $search]);
    }

    public function add_view(Request $request)
    {
        return view('/content/video/add')->with('message','');
    }

    public function add(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'video' => 'required',


        ]);

        if($validator->fails() ){
            return redirect(route('add_video_view'))
                        ->withErrors($validator)
                        ->withInput();
        }

        if (substr($data['video'], 0, 7) == "http://"){
            $res = "http";
        }elseif(substr($data['video'], 0, 8) == "https://"){
            $res = "https";
        }else{
            $res= "";
        }

        if($res == ''){
            return view('/content/video/add')->with('message','Video Link is not valid..Please check..');
             
        }
            
       
        $add = new Video();
        $add->title = $data['title'];
        $add->url = $data['video'];
        $add->save();

        return redirect('/videos');
    }

    public function edit_view(Request $request,$id)
    {
        $video = Video::find($id);
        return view('/content/video/edit')->with('edit',$video)->with('message','');
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(),[
            'title' => 'required',
        ]);

        if($validator->fails() ){
            return redirect(route('edit_video_view',[$data['id']]))
                        ->withErrors($validator)
                        ->withInput();
        }
        $video = Video::find($data['id']);
        
        if (substr($data['video'], 0, 7) == "http://"){
            $res = "http";
        }elseif(substr($data['video'], 0, 8) == "https://"){
            $res = "https";
        }else{
            $res= "";
        }

        if($res == ''){
            return view('/content/video/edit')->with('edit',$video)->with('message','Video Link is not valid..Please check..');
             
        }

        $edit = Video::find($data['id']);
        $edit->title = $data['title'];
        $edit->url = $data['video'];
        $edit->save();

        return redirect('/videos');
    }

    public function delete(Request $request, $id){

        Video::where('id',$id)->delete();

        return redirect('/videos');
    }



}
