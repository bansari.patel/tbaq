<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Score extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'school_id', 'score','round','competition','interschool_challenge','image','location_id'
    ];

    public function school(){
        return $this->hasOne('App\School','id','school_id');
    }

    public function location(){
        return $this->hasOne('App\Location','id','location_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
