<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Feedback extends Authenticatable
{
    use Notifiable;
    protected $table = 'feedbacks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'school_id', 'image','kits_used_time','kits_used_by_student','kits_photo_issue','feedback_option','purchasing_kit'
    ];

    public function school(){
        return $this->hasOne('App\School','id','school_id');
    }


    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function image(){
        return $this->hasMany('App\FeedbackImage','feedback_id','id');
    }
}
