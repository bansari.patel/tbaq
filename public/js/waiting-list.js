(function () {

	get("media/js/config.json")
	.then(x => x["waitingListEndpoint"])
	.then(setupWaitingList)
	.catch(function(x) {console.log(x)});

	function setupWaitingList(endPoint){
		var form = document.querySelector("#waitingListForm");
		form.addEventListener("submit", function (e) {
			e.preventDefault();
			e.stopPropagation();
			hideError()
			.then(showLoader)
				.then(() => 
					Array.from(form.querySelectorAll("*[name]:not([type='submit']), select"))
					.reduce(function (state, next) { 
						state[next.getAttribute("name")] = next.value; 
						return state;
					}, {}))
				.then(data => post(endPoint, data))
				.then(showThankYou)
				.catch(showError)
				.then(hideLoader);
		});
	}	
	function showError(){
		return resolveAfter(500, () => document.querySelector(".errorMessage").classList.add("show"));
	}
	function hideError(){
		return resolveAfter(500, () => document.querySelector(".errorMessage").classList.remove("show"));
	}

	function showLoader(){
		return resolveAfter(500, () => {
			var loader = document.querySelector(".loader");
			loader.classList.add("show");
			window.setTimeout(() => loader.classList.add("visible"), 50);
		});
	}
	function hideLoader(){
		return resolveAfter(500, () => {
			var loader = document.querySelector(".loader");
			loader.classList.remove("visible");
			window.setTimeout(() => loader.classList.remove("show"), 450);
		});
	}
	function showThankYou(){
		return resolveAfter(500, () => {
			document.querySelector("#waitingListForm").classList.remove("show");
			document.querySelector("#waitingListThankYou").classList.add("show");
		});
	}

	function resolveAfter(millisecond, func){
		return new Promise(function(resolve, reject){
			func();
			window.setTimeout(resolve, millisecond);
		});
	}

	function post(url, data) {
		return new Promise(function (resolve, reject) { 
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function () {
				if (xmlhttp.status == 200) { resolve(xmlhttp.response); }
				else if (xmlhttp.status != 0) { 
					reject(xmlhttp.response); 
				}
			};
			xmlhttp.open("post", url, true);
			xmlhttp.setRequestHeader("Content-Type", "application/json");
			xmlhttp.send(JSON.stringify(data));
		});
	}

	function get(url) {
		return new Promise(function (resolve, reject) {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function () {
				if (xmlhttp.status == 200) {
					setTimeout(function () { return resolve(JSON.parse(xmlhttp.response)) }, 10);
				}
				else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
			};
			xmlhttp.open("get", url, true);
			xmlhttp.send();
		});
	}
}());
