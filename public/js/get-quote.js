function post(url, data) {
return new Promise(function(resolve, reject){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 202){ resolve(xmlhttp.response);}
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
    };
    xmlhttp.open("post", url, true);
    xmlhttp.setRequestHeader("Content-Type","application/json");
    xmlhttp.send(JSON.stringify(data));
});
}


var submissionURL = "";

function showLoader(){
    var loader = document.querySelector(".loader");
    loader.classList.add("show");
    window.setTimeout(_ => loader.classList.add("visible"), 50);
}

function hideLoader(){
    var loader = document.querySelector(".loader");
    loader.classList.remove("visible");
    window.setTimeout(_ => loader.classList.remove("show"), 500);
}

function getQuote(form){
    showLoader();
    var formData = ['name','email','postcode']
        .map(x => ({key:x, value: form.querySelector("input[name='"+x+"']").value}))
        .reduce((prev, current)=> {
            prev[current.key] = current.value;
            return prev;
        }, {});
    post(submissionURL, formData)
        .then(_ => {
            hideLoader();
            document.getElementById("thankYou").classList.add("show");
        })
        .catch(x => {
            alert("Unable to record your detail. Please try again later.");
            hideLoader();
        });
}

function get(url) {
    return new Promise(function(resolve, reject){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 200){ 
          setTimeout(()=>  resolve(JSON.parse(xmlhttp.response)), 10);
        }
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
      };
      xmlhttp.open("get", url, true);
      xmlhttp.send();
    });
}


var thankYou = document.createElement("div");
var thankMessage = document.createElement("p");
var button = document.createElement("button");
button.innerHTML = "Okay";
button.addEventListener("click",e => thankYou.classList.remove("show"));
thankMessage.innerHTML = "Thank you for your message. We'll get back to you soon!";
thankYou.appendChild(thankMessage);
thankYou.appendChild(button);
thankYou.setAttribute("id","thankYou");
document.querySelector("body").appendChild(thankYou);

get("media/js/config.json").then(x => {
    submissionURL = x['queryUrl'];
    Array.from(document.querySelectorAll(".get-quote-form"))
    .map(x => x.addEventListener("submit", e => {
        e.preventDefault();
        e.stopPropagation();
        getQuote(x);
    }));
});