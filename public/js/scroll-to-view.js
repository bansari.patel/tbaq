var links = Array.from(document.querySelectorAll("a"));

links.filter(a => a.getAttribute("href") != undefined && a.getAttribute("href").indexOf("#") == 0)
  .map(a => a.addEventListener("click", function(e){
    e.preventDefault();
    e.stopPropagation();
    var name= a.getAttribute("href").replace("#","");
    var anchor = document.querySelector("a[name='"+name+"']");
    if(anchor != undefined){
      anchor.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }));

