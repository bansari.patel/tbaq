Array.from(document.querySelectorAll(".monciergeCost")).map(x => x.addEventListener("click", function(e){
    e.preventDefault();
    e.stopPropagation();
    var el = document.getElementById("monciergeBenefit");
    el.classList.add("selected");
    if(el.classList.contains("selected")){
        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
}));

Array.from(document.querySelectorAll(".membershipCost")).map(x => x.addEventListener("click", function(e){
    e.preventDefault();
    e.stopPropagation();
    var el = document.getElementById("membershipBenefit");
    el.classList.add("selected");
    if(el.classList.contains("selected")){
        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
}));

document.querySelector("#membershipBenefit button").addEventListener("click", function(e){
    var el = document.getElementById("membershipBenefit")
    el.classList.toggle("selected");
    if(!el.classList.contains("selected")){
        document.getElementById("membershipCost").scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
});

document.querySelector("#monciergeBenefit button").addEventListener("click", function(e){
    var el = document.getElementById("monciergeBenefit");

    el.classList.toggle("selected");
    if(!el.classList.contains("selected")){
        document.getElementById("monciergeCost").scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
});

Array.from(document.querySelectorAll("#estimate input"))
    .forEach(x => {
        x.addEventListener("keyup", updateCosting);
        x.addEventListener("change", updateCosting);
        x.addEventListener("mouseup", updateCosting);
    });

function updateCosting(){
    var panel = document.querySelector("#estimate");
    var totalCost = Array.from(panel.querySelectorAll("#estimate input[type='number']"))
        .map(x => {
            var name = x.getAttribute("name");
            var hour = parseFloat(x.value);
            hour = isNaN(hour) ? 0 : hour;
            var rate = parseFloat(x.getAttribute("data-rate"));
            var output = panel.querySelector("span[data-for='"+name+"']");
            var cost = hour * rate;
            output.innerHTML = cost.toFixed(2);
            output.setAttribute("data-value", cost);
            return cost;
        })
        .reduce((state, next) => state + next, 0);
    panel.querySelector(".weekTotal").innerHTML = totalCost.toFixed(2);
}
updateCosting();
var panel = document.querySelector("#estimate");

panel.querySelector(".showMoreLink").addEventListener("click",  e=>{
    Array.from(document.querySelectorAll(".moreHour")).forEach(x => x.classList.toggle("hide"));
    panel.querySelector(".showMoreLink").classList.add("hide");
})