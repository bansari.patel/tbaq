(() => {
    var target = document.querySelector(".visitor-insight");
    Array.from(target.querySelectorAll("button"))
    .map(x => x.addEventListener("click", e => {
        target.querySelector(".visitor-insight ul").classList.add("hide");
        target.querySelector(".visitor-insight .outcome").classList.add("show");
        var intent = x.getAttribute("data-intent");
        var suggestion = target.querySelector(".visitor-insight *[data-for-intent='"+intent+"']");
        if(suggestion){ suggestion.classList.add("show"); }
        var questions = target.querySelector(".question");
        if(questions){ questions.classList.add("hide"); }
        target.querySelector("h2 span[data-intent-status='known']").classList.add("show");
        target.querySelector("h2 span[data-intent-status='unknown']").classList.add("hide");
    }));
})();
