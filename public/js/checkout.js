var handler = null;
var coupon = null;

var moreHourOption = document.querySelector("a#moreHours");

var premadeOptions = Array.from(document.querySelectorAll("ul.premade-options > li"));
if(premadeOptions.length > 0){
  setUpPremadeOptions();
}


function setupCouponLookup(url) {
  var codeInput = document.querySelector("input[name='campaigncode']");

  function updateCoupon() {
    coupon = null;
    document.querySelector("input[type='submit']").setAttribute("value", "Join for only $149");
    get(url.replace("{coupon}", codeInput.value))
      .then(x => {
        coupon = x;
        document.querySelector("input[type='submit']").setAttribute("value", "Join for only $" + getAmount(coupon, 14900) / 100);
      })
      .catch(e => { /* campaign code is not coupon fail silently */ });
  }

  codeInput.addEventListener("change", updateCoupon);
  updateCoupon();
}


function hideLoader() {
  return new Promise(function (resolve, reject) {
    document.querySelector(".loader").classList.remove("visible");
    setTimeout(function () {
      document.querySelector(".loader").classList.remove("show");
      resolve();
    }, 510);
  })
}

function showLoader() {
  return new Promise(function (resolve, reject) {
    document.querySelector(".loader").classList.add("show");
    setTimeout(function () {
      document.querySelector(".loader").classList.add("visible");
      resolve();
    }, 50);
  })
}

function finishWithToken(token) {
  var data = Array.from(document.querySelectorAll("#joinForm *[name]:not(*[type=radio]), #joinForm [type='radio']:checked"))
    .map(function (x) {
      return ({
        key: x.getAttribute("name"),
        value: x.value,
      })
    })
    .reduce(function (prev, current) {
      prev[current.key] = current.value;
      return prev;
    },
      {});
  data["token"] = token.id;

  post(document.querySelector('form').getAttribute("action"), data)
    .then(function () { return hideLoader(); })
    .then(function () { return document.location.href = "thankyou.html"; })
    .catch(function () {
      hideLoader().then(function () {
        alert("Unable to finish sign up process. Please send us a message, will get back to you!");
      })
    });
}

function getAmount(cp, pr) {
  if (cp.percent_off !== null) {
    return Math.round((1 - (cp.percent_off / 100)) * pr);
  } else if (cp.amount_off !== null) {
    return pr - cp.amount_off;
  } else {
    return pr;
  }
}

document.getElementById('joinForm').addEventListener('submit', function (e) {
  e.stopPropagation();
  e.preventDefault();
  if (window.ga) {
    ga('send', {
      hitType: 'event',
      eventCategory: 'Join',
      eventAction: 'paymentAttempt',
      eventLabel: 'Join mtime'
    });
  }

  var price = 14900;
  var amount = coupon !== null && coupon !== undefined
    ? getAmount(coupon, price)
    : price;

  handler.open({
    "name": 'mtime',
    "description": 'Join mtime',
    "email": document.querySelector("input[type='email']").value,
    "amount": amount,
    "currency": "aud",
    "panelLabel": "Pay {{amount}} signup fee"
  });

  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function () {
  handler.close();
});


function post(url, data) {
  return new Promise(function (resolve, reject) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.status == 200) { resolve(xmlhttp.response); }
      else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
    };
    xmlhttp.open("post", url, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(data));
  });
}

function get(url) {
  return new Promise(function (resolve, reject) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.status == 200) {
        setTimeout(function () { return resolve(JSON.parse(xmlhttp.response)) }, 10);
      }
      else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
    };
    xmlhttp.open("get", url, true);
    xmlhttp.send();
  });
}

//popuplate form with get URL
var params = window.location.search
  .replace("?", "")
  .split("&")
  .map(function (x) { return x.split("="); })
  .map(function (x) {
    var input = document.querySelector("input[name='" + x[0] + "']");
    if (input && x[1] != undefined && x[1] != "") {
      input.value = x[1];
    }
    if (x[0] == "name") {
      document.querySelector("input[name='first']").value = decodeURI(x[1]);
    }
    if (x[0] == "package") {
      var selector = "ul.premade-options li[data-value='" + x[1] + "']";
      console.log(selector);
      var premade = document.querySelector(selector);
      if (premade != null && premade != undefined) {
        hideCustomHour();
        premadeOptions.map(x => x.classList.remove("selected"));
        premade.classList.add("selected");
      } else {
        document.querySelector("input[name='hour']").value = x[1];
        showCustomHour();
      }
    }
  });


var configName = document.location.href.indexOf("http://localhost") == 0 ? "local.config.json" : "config.json";
get("media/js/" + configName)
  .then(function (x) {
    setupCouponLookup(x['couponLookUpUrl']);

    handler = StripeCheckout.configure({
      key: x['mtimeStripePublishableKey'],
      image: './media/img/favicon.png',
      color: 'black',
      locale: 'auto',
      token: function (token) {
        return showLoader().then(finishWithToken(token));
      }
    });
    document.getElementById("joinForm").setAttribute("action", x["checkoutUrl"]);
    document.getElementById("joinButton").classList.add("show");

  }).catch(function (x) { console.log(x) });

var daySelectInput = document.querySelector("#daySelect");
if (daySelectInput !== null && daySelectInput !== undefined) {
  setUpDaysSelect();
}


function showCustomHour() {
  document.querySelector("label[for='hour']").classList.add("show");
  document.querySelector("input[name='hour']").classList.add("show");
  moreHourOption.classList.remove("show");
  premadeOptions.map(x => x.classList.remove("selected"));
}

function hideCustomHour() {
  document.querySelector("label[for='hour']").classList.remove("show");
  document.querySelector("input[name='hour']").classList.remove("show");
  moreHourOption.classList.add("show");
}

function setUpPremadeOptions() {
  
  Array.from(document.querySelectorAll("form ul div"))
  .map(function(x) { x.addEventListener("click", function(e) { x.querySelector("input[type='radio']").click(); })});
  
  moreHourOption.addEventListener("click", showCustomHour);
  hideCustomHour();

  

  premadeOptions.map(x => x.addEventListener("click", function (e) {
    hideCustomHour();
    e.currentTarget.classList.toggle('selected');
    premadeOptions.filter(x => x != e.currentTarget).map(y => y.classList.remove("selected"));
    var hourInput = document.querySelector("input[name='hour']");
    hourInput.value = e.currentTarget.getAttribute("data-value");
    hourInput.dispatchEvent(new Event("change"));
    console.log("fun");
  }));
}

function setUpDaysSelect() {
  var dayIndexes = {
    "sun": 1,
    "mon": 2,
    "tue": 3,
    "wed": 4,
    "thu": 5,
    "fri": 6,
    "sat": 7,
  }
  var daysItems = [];
  var select = document.querySelector("#daySelect");
  var addDayButton = document.querySelector("#addDay");
  select.addEventListener("change", function (e) {
    if (select.value != "" && select.value != undefined && select.value != null) {
      addDayButton.removeAttribute("disabled");
      select.classList.add("has-value");
    } else {
      addDayButton.setAttribute("disabled", "true");
      select.classList.remove("has-value");
    }
  });

  Array.from(document.querySelectorAll("#days li")).map(x => x.style.display = "none");
  addDayButton.addEventListener("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    var dayToAdd = document.querySelector("#daySelect").value;
    document.querySelector("#days li[data-value='" + dayToAdd + "']").style.display = "flex";
    var select = document.querySelector("#daySelect");
    daysItems.push(select[select.selectedIndex]);
    select.remove(select.selectedIndex);
    addDayButton.setAttribute("disabled", "true");
  });

  Array.from(document.querySelectorAll("#days li button[data-value]")).map(x => x.addEventListener("click", function (e) {
    e.preventDefault();
    e.stopPropagation();
    var day = e.srcElement.getAttribute("data-value");
    document.querySelector("input[name='" + day + "Start']").value = "";
    document.querySelector("input[name='" + day + "End']").value = "";
    var option = daysItems.find(x => x.value == day);
    select.add(option, dayIndexes[day]);
    select.value = "";
    daysItems.splice(daysItems.indexOf(option), 1);
    document.querySelector("#days li[data-value='" + day + "']").style.display = "none";
  }));

}

setupNSWLaunchDeal();
function setupNSWLaunchDeal(){
  var stateInput = document.querySelector("input[name='state']");
  var hourInput = document.querySelector("input[name='hour']");
  var campaignCode = document.querySelector("input[name='campaigncode']");
  var defaultCoupon = campaignCode.value; 
  stateInput.addEventListener("change", updateNSWLaunchDeal);
  hourInput.addEventListener("change", updateNSWLaunchDeal)

  function updateNSWLaunchDeal(){
    var stateValue = stateInput.value.toLocaleLowerCase();
    var hour = hourInput.value;
    var isFromNSW = stateValue == "nsw" || stateValue == "new south wales";
    if(isFromNSW && hour >= 6){
      campaignCode.value = "NSW";
    }else{
      campaignCode.value = defaultCoupon;
    }
    campaignCode.dispatchEvent(new Event("change"));

  }
}

var serviceablePostcode = ["2046","2015","2100","2218","2038","2205","2064","2193","2131","2093","2041","2216","2019","2207","2191","2023",
"2192","2085","2217","2209","2113","2026","2022","2137","2024","2134","2136","2062","2050","2194","2069","2068",
"2021","2067","2036","2008","2190","2206","2031","2138","2034","2090","2065","2132","2133","2096","2032","2027",
"2010","2000","2099","2114","2112","2028","2030","2047","2203","2071","2070","2018","2122","2011","2042","2043",
"2867","2094","2037","2087","2086","2111","2072","2045","2060","2140","2127","2110","2220","2033","2208","2061",
"2195","2066","2040","2049","2095","2035","2204","2020","2088","2089","2055","2063","2073","2009","2016","2141",
"2029","2196","2039","2092","2044","2048","2135","2130","2006","2007","2017","2025","3067","3206","3078","3143",
"3032","3183","3103","3056","3057","3121","3124","3127","3126","3053","3054","3162","3145","3161","3068","3058",
"3066","3008","3002","3185","3184","3040","3065","3031","3011","3146","3122","3123","3101","3144","3012","3000",
"3039","3015","3051","3070","3052","3207","3181","3072","3073","3205","3006","3141","3182","3142","3071","3043",
"3003","3013","3975","2144","2128", "3188", "3186"];

//var postcodeInput = document.querySelector("input[name='postcode']");
//postcodeInput.addEventListener("change", e => serviceablePostcode.indexOf(postcodeInput.value)==-1? cannotService():canService());
function cannotService(){
  document.querySelector("input[type='submit']").setAttribute("disabled", "true");
  document.querySelector("#noServiceWarning").style.display = "block";
}
function canService(){
  document.querySelector("input[type='submit']").removeAttribute("disabled");
  document.querySelector("#noServiceWarning").style.display = "none";
}

canService()