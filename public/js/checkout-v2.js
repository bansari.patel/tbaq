(function(){
  var coupon = null;

  function hideLoader() {
    return new Promise(function (resolve, reject) {
      document.querySelector(".loader").classList.remove("visible");
      setTimeout(function () {
        document.querySelector(".loader").classList.remove("show");
        resolve();
      }, 510);
    })
  }

  function showLoader() {
    return new Promise(function (resolve, reject) {
      document.querySelector(".loader").classList.add("show");
      setTimeout(function () {
        document.querySelector(".loader").classList.add("visible");
        resolve();
      }, 50);
    })
  }

  function post(url, data) {
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.status == 200 && xmlhttp.readyState==4) { 
          resolve(xmlhttp.response); 
        }
        else if (xmlhttp.status != 200 && xmlhttp.readyState==4) { 
          reject({status: xmlhttp.status, response: xmlhttp.response}); 
        }
      };
      xmlhttp.open("post", url, true);
      xmlhttp.setRequestHeader("Content-Type", "application/json");
      xmlhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
      xmlhttp.send(JSON.stringify(data));
    });
  }

  function get(url) {
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.status == 200) {
          setTimeout(function () { return resolve(JSON.parse(xmlhttp.response)) }, 10);
        }
        else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
      };
      xmlhttp.open("get", url, true);
      xmlhttp.send();
    });
  }


  const configName = document.location.href.indexOf("http://localhost") == 0 ? "local.config.json" : "config.json";
  get("media/js/" + configName)
    .then(function (config) {
      const reportJoin= function(){
        if (window.ga) {
          ga('send', {
            hitType: 'event',
            eventCategory: 'Join',
            eventAction: 'paymentAttempt',
            eventLabel: 'Join mtime'
          });
        }
      }; 

      const getFormData = ()=> ({
        firstName: document.querySelector("input[name='first']").value,
        lastName: document.querySelector("input[name='last']").value,
        address:{
          physicalAddress:{
            streetAddress:  document.querySelector("input[name='address']").value,
            suburb: document.querySelector("input[name='city']").value,
            state: document.querySelector("input[name='state']").value,
            postcode: document.querySelector("input[name='postcode']").value,
            countryCode: 'au'
          },
          digitalAddress:{
            emailAddress: document.querySelector("input[name='email']").value,
            phoneNumber: document.querySelector("input[name='phone']").value
          },
        },
        estimatedHour: Number.parseInt(document.querySelector("input[name='hour']").value),
        couponCode: document.querySelector("input[name='campaigncode']").value
      });
      const showErrorMessage = function(message){
        document.querySelector("#errorMessage p").innerHTML = message;
        document.getElementById("errorMessage").classList.add("show");
        return Promise.resolve();
      }
      const hideErrorMessage = function(){
        document.getElementById("errorMessage").classList.remove("show");
        return Promise.resolve();
      }

      const checkout = () =>
        hideErrorMessage()
        .then(showLoader)
        .then(() => 
          post(config.checkoutV2, getFormData())
          .then(x => {
            var json = JSON.parse(x);
            Stripe(config.mtimeStripePublishableKey).redirectToCheckout({ sessionId: json.id });
          })
          .then(result => result.error ? alert(result.error.message) : null)
          .then(reportJoin))
          .catch(e => {
            if(e.status == 400){
              return showErrorMessage(e.response);
            }else if(e.status == 500){
              return showErrorMessage("Unknown error occured while you was trying to sign up. Please try again later. Sorry for the inconvenienct, we will be in touch!")
            }
          })
        .then(hideLoader);

      document.getElementById("joinForm").addEventListener("submit", function(e){
        e.preventDefault();
        e.stopPropagation();
        checkout();
      })
      document.getElementById("joinButton").classList.add("show");

    }).catch(console.log);

  const premadeOptions = Array.from(document.querySelectorAll("ul.premade-options > li"));

  //popuplate form with get URL
  window.location.search
    .replace("?", "")
    .split("&")
    .map(function (x) { return x.split("="); })
    .map(function (x) {
      var key = x[0]; var val=x[1];
      var input = document.querySelector("input[name='" +key+ "']");
      if (input && val != undefined && val != "") {
        input.value = val;
      }
      if (val == "name") {
        document.querySelector("input[name='first']").value = decodeURI(val);
      }
      if (key == "package") {
        var selector = "ul.premade-options li[data-value='" + val + "']";
        var premade = document.querySelector(selector);
        if (premade != null && premade != undefined) {
          hideCustomHour();
          premadeOptions.map(y => y.classList.remove("selected"));
          premade.classList.add("selected");
        } else {
          document.querySelector("input[name='hour']").value = val;
          showCustomHour();
        }
      }
    });


  function setUpPremadeOptions() {
    if(premadeOptions.length<1){return;}
    Array.from(document.querySelectorAll("form ul div"))
    .map(function(x) { x.addEventListener("click", function(e) { x.querySelector("input[type='radio']").click(); })});
    
    var moreHourOption = document.querySelector("a#moreHours");
    moreHourOption.addEventListener("click", showCustomHour);
    hideCustomHour();

    function showCustomHour() {
      document.querySelector("label[for='hour']").classList.add("show");
      document.querySelector("input[name='hour']").classList.add("show");
      moreHourOption.classList.remove("show");
      premadeOptions.map(x => x.classList.remove("selected"));
    }

    function hideCustomHour() {
      document.querySelector("label[for='hour']").classList.remove("show");
      document.querySelector("input[name='hour']").classList.remove("show");
      moreHourOption.classList.add("show");
    }

    premadeOptions.map(x => x.addEventListener("click", function (e) {
      hideCustomHour();
      e.currentTarget.classList.toggle('selected');
      premadeOptions.filter(x => x != e.currentTarget).map(y => y.classList.remove("selected"));
      var hourInput = document.querySelector("input[name='hour']");
      hourInput.value = e.currentTarget.getAttribute("data-value");
      hourInput.dispatchEvent(new Event("change"));
    }));
  }
  setUpPremadeOptions();
}());
