(function(){

  const viewedKey = "TimeQuizPopUpViewed";
  const mobileWaitTime = 30000;
  const desktopWaitTime = 2000;
  var popupContainer;
  function checkIfMobileOrTablet () {
    return !matchMedia('(pointer:fine)').matches;
  };


  var mouseLeaveHandler = () => {
    showPopup();
  };
  if(checkIfMobileOrTablet()){
    if(window.localStorage.getItem(viewedKey) !== "true"){
      window.setTimeout(()=>showPopup(), mobileWaitTime);
    }
  }else{
    if(window.localStorage.getItem(viewedKey) !== "true"){
      window.setTimeout(() => {
        document.addEventListener("mouseleave", mouseLeaveHandler);
      }, desktopWaitTime);
    }
  }


  function createPopup(){
    popupContainer = document.createElement("div");
    popupContainer.style="display: none; opacity: 0; transition: opacity 0.5s linear; position:fixed; width: 100%; height: 100%; top: 0; left: 0; background: rgba(255,255,255,0.9); z-index: 20; flex-direction: column;align-items: center;justify-content: center;";
    var closeButton = document.createElement("button");
    closeButton.innerHTML = "Close";
    closeButton.addEventListener("click", hidePopup);
    closeButton.style="margin-top:1em;padding: 0.5em 1em; font-size: 1.25em; border-radius: 4px; border: 1px solid #ff6a00; background:transparent; color: #ff6a00;";
    
    var popup = document.createElement("div");
    popup.style="width: 100%; height: 70vh; position: relative;";
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id","JotFormIFrame-90141060682852");
    iframe.setAttribute("title","mtime Quiz : How much time do I need?");
    iframe.setAttribute("onload","window.parent.scrollTo(0,0)");
    iframe.setAttribute("allowtransparency","true");
    iframe.setAttribute("allowfullscreen","true");
    iframe.setAttribute("allow","geolocation; microphone; camera");
    iframe.setAttribute("src","https://form.jotform.co/90141060682852");
    iframe.setAttribute("frameborder","0");
    iframe.setAttribute("scrolling","no");
    iframe.style=" min-width: 100%; height:100%;border:none;";
    
    popup.appendChild(iframe);
    popupContainer.appendChild(popup);
    popupContainer.appendChild(closeButton)
    document.body.appendChild(popupContainer);
    popupContainer.addEventListener("click", hidePopup);
  }

  function showPopup(){
    document.removeEventListener("mouseleave", mouseLeaveHandler);
    window.localStorage.setItem(viewedKey, "true");
    
    if(popupContainer){
      popupContainer.style.display = "flex";
      window.setTimeout(()=> popupContainer.style.opacity=1, 50);
    }
  }

  function hidePopup(){
    if(popupContainer){
      popupContainer.style.opacity=0;
      window.setTimeout(()=> popupContainer.style.display = "none", 600);
    }
  }

  createPopup();

}());


