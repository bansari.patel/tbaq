

var currentMenu = document.location.pathname.replace("/","");

Array.from(document.querySelectorAll("nav[role='navigation'] a"))
    .filter(x => x.getAttribute("href").indexOf(currentMenu) != -1)
    .map(x => {
        x.classList.add("selected");
        var list = x.closest("ul")
        if(list){
            var item = list.closest("li");
            if(item){
                var button = item.querySelector("button");
                if(button) { button.classList.add("selected"); }
            }
        }
    });

Array.from(document.querySelectorAll("nav[role='navigation'] li"))
    .map(x => {
        var button = x.querySelector("button");
        if(button){
            button.addEventListener("click", e => x.classList.toggle("show"))
        }
    })

window.addEventListener("click", e =>{
    if(!event.target.matches("nav[role='navigation'] button")){
        Array.from(document.querySelectorAll('nav[role="navigation"] li.show'))
            .forEach(x => x.classList.remove("show"));
    }
});

function createWarning(){
    return new Promise((resolve, reject)=>{
        var container = document.createElement("section");
        container.classList.add("call-warning-container");

        var body = document.createElement("div");
        container.appendChild(body);
        var p = document.createElement("p");
        p.innerHTML= "Thank you for calling mtime. We are not currently in office at this time. You can leave us a message via our contact form.";
        body.appendChild(p);

        var contact = document.createElement("a");
        contact.setAttribute("href","contact.html");
        "button white".split(" ").map(x => contact.classList.add(x));
        contact.innerHTML = "Send a message";
        body.appendChild(contact);
        var close = document.createElement("button");
        close.innerHTML = "Okay";
        body.appendChild(close);
        close.addEventListener("click", closePopup);
        container.addEventListener("click", closePopup);
        function closePopup(e){
            container.classList.remove("show");
            window.setTimeout(e=>{
                document.body.removeChild(container);
            }, 500);
        }
        document.body.appendChild(container);
        window.setTimeout(e=> {
            container.classList.add("show");
            resolve();
        }, 50);
    })
}

function copyrightYear(){
    var year = new Date().getFullYear();
    Array.from(document.querySelectorAll("#copyrightYear")).map(x => x.innerHTML = year);
}

copyrightYear();

function createSignUpPanel(){
    var content = {
      "title": "Sign up",
      "description": "Pick your service and start your moncierge experience",
      "items": [
        { 
          "name": "At home support", 
          "link": "/join.html",
          "image": "/media/img/moncierge-illustration.svg"
        },
        { 
          "name": "Education support", 
          "link": "/waiting-list-education-support.html",
          "image": "/media/img/homeschool.svg"
        },
      ],
      "closeAction": "Maybe later"
    }

    var section = document.createElement("section");
    section.setAttribute("id", "signUpNow");
    
    var container = document.createElement("div");
    section.appendChild(container);
    
    var title = document.createElement("h2");
    title.innerHTML = content.title;
    container.appendChild(title);
    
    var description = document.createElement("p");
    description.innerHTML = content.description;
    container.appendChild(description);
   
    var list = document.createElement("ul");
    container.appendChild(list);
    
    content.items.map(function(x){
      var item = document.createElement("li");
      var link = document.createElement("a");
      link.setAttribute("href", x.link);
      item.appendChild(link);
      
      var image = document.createElement("img");
      image.setAttribute("src", x.image);
      image.setAttribute("alt", x.name);
      link.appendChild(image);
      
      var productNameContainer = document.createElement("div");
      productNameContainer.classList.add("name");
      link.appendChild(productNameContainer);

      var logo = document.createElement("span");
      logo.classList.add("logo");
      logo.innerHTML = "mtime";
      productNameContainer.appendChild(logo);

      var productName = document.createElement("span");
      productName.classList.add("product");
      productName.innerHTML = x.name;
      productNameContainer.appendChild(productName);

      list.appendChild(item);
    });

    var closeButton = document.createElement("button");
    closeButton.getAttribute("id", "closeSignUpNow");
    closeButton.innerHTML = content.closeAction;
    container.appendChild(closeButton);

    function showSignUpPanel(){
      section.classList.add("show");
      window.setTimeout(function(){ section.classList.add("visible"); }, 50);
    }

    closeButton.addEventListener("click", function(){
      section.classList.remove("visible");
      window.setTimeout(function(){ section.classList.remove("show"); }, 250);
    });

    document.body.appendChild(section);
    
    Array.from(document.querySelectorAll("#signUpButton, nav[role='navigation'] a, body > footer > ul > li > a"))
      .filter(x => x.getAttribute("href").indexOf("#services") != -1)
      .map(x => x.addEventListener("click", function(e){
        e.preventDefault();
        e.stopPropagation();
        showSignUpPanel();
      }));
  }

  createSignUpPanel();