var stepName = document.location.hash.replace("#","").replace(/-/g," ");
if(stepName != ""){
    var steps = Array.from(document.querySelectorAll(".three-steps ol ol li"));
    var currentSteps = steps
        .filter(x =>  {
            console.log("comparing " + x.innerHTML.toLowerCase() + " with " + stepName)
            return x.innerHTML.toLowerCase() == stepName});
    currentSteps
        .map(x => x.classList.add("current"));
    
    var bigSteps = Array.from(document.querySelectorAll(".three-steps>ol>li"));
    var currentBigStep = bigSteps.find(x => x.querySelector("ol").contains(currentSteps[0]))
    
    if(currentBigStep != null){
        currentBigStep.classList.add("current");
    }


    var i = 0;
    while(true){
        if(steps[i].classList.contains("current")){
            break;
        }
        steps[i].classList.add("complete");
        i++;
        if(i == steps.length - 1){
            break;
        }
    }

    Array.from(document.querySelectorAll("#nextStep>ul>li"))
        .filter(x => x.querySelector("h2").innerHTML.toLowerCase() != stepName)
        .map(x => x.classList.add("hide"));

}