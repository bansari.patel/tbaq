document.querySelector("nav[role='navigation'] button").addEventListener("click", function(){
  document.querySelector(".top-navigation-container").classList.toggle("selected");
})

Array.from(document.querySelectorAll("nav[role='navigation'] a"))
  .filter(a => document.location.pathname.indexOf(a.href) != -1 )
  .map(a => a.classList.add("selected"));