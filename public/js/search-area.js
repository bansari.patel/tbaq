(function(){
    

    var cancelBlur = false;
    var searchForm = document.querySelector("#searchForm");
    var areas = getData();
    searchForm.addEventListener("submit", function(e) {
        e.preventDefault();
        e.stopPropagation();
        search();
        
        // select top result
        var topResult = document.querySelector("#searchSuggestion li");
        if(topResult !== undefined && topResult !== null){
            topResult.click();
        }
    });
    var textInput = searchForm.querySelector("input[type='text']");
    textInput.addEventListener("keyup", function(e){
        e.preventDefault();
        e.stopPropagation();
        if(e.keyCode != 13){
            search();
        }
    });

    textInput.addEventListener("blur", function(e){
        e.preventDefault();
        e.stopPropagation();
        setTimeout(function(){
            var result = document.querySelector("#searchSuggestion");
            result.innerHTML = "";
        }, 400);
    });
    

    Array.from(document.querySelectorAll("section button.toggle"))
        .map(x => x.addEventListener("click", function(e){
            x.parentNode.classList.toggle("expand");
        }))

    function search(){
        var keyword = searchForm.querySelector("input[type='text']").value;
        if(keyword !== null && keyword !== undefined && keyword !== ""){
            Promise.resolve(normalise(keyword))
                .then(findItem)
                .then(displaySuggestion);
        }else{
            clearSearchSuggestion();
        }
    }

    function findItem(keyword){
        return areas.filter(x => x.name.indexOf(keyword) != -1  || x.zip.indexOf(keyword) != -1);
    }

    function displaySuggestion(items){
        var result = document.querySelector("#searchSuggestion");
        result.innerHTML = "";
        items.forEach(item => {
            var newNode =  item.node.cloneNode(true);
            newNode.addEventListener("click", _ => {
                console.log("show result for " + item);
                showResult(item);
            });
            result.appendChild(newNode);
        });
    }

    function showResult(item){
        document.querySelector("#resultCallToAction").classList.add("show");
        var resultPanelSelector = "." + item.level + "-result";
        Array.from(document.querySelectorAll("#resultCallToAction li")).map(x => x.classList.remove("show"));
        var result = document.querySelector(resultPanelSelector);
        result.querySelector(".name").innerHTML = item.node.querySelector("address").innerHTML;
        result.classList.add("show");
    }

    function clearSearchSuggestion(){
        var result = document.querySelector("#searchSuggestion");
        result.innerHTML = "";   
    }

    function normalise(term){
        return term.toLowerCase().replace(" ", "");
    }

    function getData(){
        return Array.from(document.querySelectorAll("ul.serviceAreas"))
            .map(ul => Array.from(ul.querySelectorAll("li"))
                    .map(li => {
                        var address = li.querySelector("address");
                        return {
                            "name": normalise(address.innerHTML),
                            "zip": address.getAttribute("data-zip"),
                            "level": ul.getAttribute("data-level"),
                            "node": li
                        };
                }))
            .reduce((state, next) => state.concat(next));
    }

})();