function post(url, data) {
return new Promise(function(resolve, reject){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 202){ resolve(xmlhttp.response);}
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
    };
    xmlhttp.open("post", url, true);
    xmlhttp.setRequestHeader("Content-Type","application/json");
    xmlhttp.send(JSON.stringify(data));
});
}


var submissionURL = "";

get("media/js/config.json").then(x => {
    submissionURL = x['queryUrl'];
    Array.from(document.querySelectorAll("form"))
    .map(x => x.addEventListener("submit", e => {
        e.preventDefault();
        e.stopPropagation();
        getQuote(x);
    }));
});

function showThankYou(){
    document.querySelector("#thankYou").classList.add("show");
}

document.querySelector("#thankYou button").addEventListener("click", function(){
    document.querySelector("#thankYou").classList.remove("show");
});

function showLoader(){
    var loader = document.querySelector(".loader");
    loader.classList.add("show");
    window.setTimeout(_ => loader.classList.add("visible"), 50);
}

function hideLoader(){
    var loader = document.querySelector(".loader");
    loader.classList.remove("visible");
    window.setTimeout(_ => loader.classList.remove("show"), 500);
}

function getQuote(form){
    showLoader();
    var formData = ['name','email','postcode', 'message']
        .map(x => ({key:x, value: form.querySelector("form *[name='"+x+"']").value}))
        .reduce((prev, current)=> {
            prev[current.key] = current.value;
            return prev;
        }, {});
    post(submissionURL, formData)
        .then(_ => {
            ['name','email','postcode', 'message']
                .map(x => document.querySelector("form *[name='"+x+"']"))
                .map(x => x.value = "");
            hideLoader();
            showThankYou();
        })
        .catch(x => {
            alert("Unable to record your detail. Please try again later.");
            hideLoader();
        });
}

function get(url) {
    return new Promise(function(resolve, reject){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 200){ 
          setTimeout(()=>  resolve(JSON.parse(xmlhttp.response)), 10);
        }
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
      };
      xmlhttp.open("get", url, true);
      xmlhttp.send();
    });
  }