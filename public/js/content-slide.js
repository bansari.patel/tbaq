function createSlider(list){
  var selectedClass = "selected";
  var length = list.querySelectorAll("li").length;
  var buttonList = document.createElement("nav");
  buttonList.classList.add("slide-nav");
  buttonList.setAttribute("aria-hidden", "true");
  list.querySelector("li").classList.add(selectedClass);

  var navigate = function(index){
    list.querySelector("li.selected").classList.remove(selectedClass);
    list.querySelector("li:nth-child("+(index+1)+")").classList.add(selectedClass);
    buttonList.querySelector("button.selected").classList.remove(selectedClass);
    buttonList.querySelector("button:nth-child("+(index+1)+")").classList.add(selectedClass);
  };

  createRange(0, length)
    .map(function(i){
      var button = document.createElement("button");
      button.addEventListener("click", ()=> navigate(i));
      return button;
    })
    .reduce(function(previous, current, index, array){
      previous.appendChild(current);
      return previous;
    }, buttonList);
  list.parentNode.insertBefore(buttonList, list.nextSibling);
  buttonList.querySelector("button").classList.add(selectedClass);
}

function createRange(start, stop){
  var output = [];
  for(var i=start;i<stop;i++){
    output[i]=i;
  }
  return output;
}

Array.from(document.querySelectorAll(".slidable")).map(createSlider);
