(function (){

    function post(url, data) {
        return new Promise(function(resolve, reject){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
            if(xmlhttp.status == 200){ resolve(xmlhttp.response);}
            else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
            };
            xmlhttp.open("post", url, true);
            xmlhttp.setRequestHeader("Content-Type","application/json");
            xmlhttp.send(JSON.stringify(data));
        });
    }

    function get(url) {
        return new Promise(function(resolve, reject){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.status == 200){ 
            setTimeout(function(){  return resolve(JSON.parse(xmlhttp.response)) }, 10);
            }
            else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
        };
        xmlhttp.open("get", url, true);
        xmlhttp.send();
        });
    }

    var city = document.querySelector("*[name='city']");
    city.addEventListener("change", function(e){
    document.querySelector("*[name='postcode']").value = city.querySelector("option[value='"+city.value+"']").getAttribute("data-zip");
    });

    get("media/js/config.json")
    .then(x => setupReactivation(x['reactivateEndpoint']));

    function setupReactivation(endpoint){
        return new Promise((resolve, reject) => {
            var form = document.getElementById('reactivateForm');
            form.addEventListener('submit', function(e) {
                e.stopPropagation();
                e.preventDefault();
                showLoader()
                    .then(e => post(endpoint, getData()))
                    .then(hideLoader)
                    .then(showThankyou)
                    .catch(showError)
                    .then(hideLoader);
            });

            form.querySelector("input[type='submit']").classList.add("show");
            resolve();        
        });
    }

    function getData(){
        return Array.from(document.querySelectorAll("#reactivateForm *[name]"))
            .reduce((state, next)=>{
                state[next.getAttribute("name")] = next.value;
                return state;
            },{});
    }

    function showLoader(){
        return new Promise((resolve, reject)=>{
            var loader = document.querySelector(".loader");
            loader.classList.add("show");
            window.setTimeout(()=>{
                loader.classList.add("visible");
                window.setTimeout(resolve, 400);
            }, 50);
        });
    }
    function hideLoader(){
        return new Promise((resolve, reject)=>{
            var loader = document.querySelector(".loader");
            loader.classList.remove("visible");
            window.setTimeout(()=>{
                loader.classList.remove("show");
                resolve();
            }, 500);
        });
    }
    function showThankyou(){
        return new Promise((resolve, reject)=>{
            document.querySelector(".thank-you").classList.add("show");
            window.setTimeout(()=>resolve(), 500);
        });
    }
    function showError(e){
        return new Promise((resolve, reject)=>{
            document.querySelector("#errorScreen").classList.add("show");
            window.setTimeout(resolve, 500);
        });
    }
    function hideError(){
        return new Promise((resolve, reject)=>{
            document.querySelector("#errorScreen").classList.remove("show");
            window.setTimeout(resolve, 500);
        });
    }

    document.querySelector("#errorScreen button").addEventListener("click", hideError);
})();