(function setupGift(){

  var config = get("media/js/config.json");

  config
    .then(x => x["pricingEndpoint"])
    .then(get)
    .then(setupPricing);

  config
    .then(x => setupCheckout(x['mtimeStripePublishableKey'], x['checkoutUrl'], x['giftEndpoint'] ));

  
  function toDisplayCost(costInCent, currency){
    return currency + " " + (costInCent / 100).toFixed(2);
  }

  function setupPricing(priceTable){
    var rate = priceTable['items'].find(x => x.type=="https://schema.mtime.com.au/product/gift/hour-rate")['value'];
    var memberCost = priceTable['items'].find(x => x.type=="https://schema.mtime.com.au/product/gift/member")['value'];
    var nonMemberCost = priceTable['items'].find(x => x.type=="https://schema.mtime.com.au/product/gift/nonmember")['value'];

    var hourInput = document.querySelector("#numberOfHour");
    var hourPriceOutput = document.querySelector("#hourPrice");
    var cardPriceOutput = document.querySelector("#cardPrice");
    var totalCostOutput = document.querySelector("#totalPrice");
    var memberCostOutput = document.querySelector("[data-schema='https://schema.mtime.com.au/product/gift/member'] .price");
    var nonmemberCostOutput = document.querySelector("[data-schema='https://schema.mtime.com.au/product/gift/nonmember'] .price");
    var checkoutButton = document.querySelector("#giftForm input[type='submit']");
    
    memberCostOutput.innerHTML = toDisplayCost(memberCost, "AUD");
    memberCostOutput.setAttribute("data-value", memberCost);
    nonmemberCostOutput.innerHTML = toDisplayCost(nonMemberCost, "AUD");
    nonmemberCostOutput.setAttribute("data-value", nonMemberCost);
    


    function updateCostOutput(){
      var hourCost = isNaN(hourInput.value) ? 0 : hourInput.value * rate;
      var card = document.querySelector(".card.selected .price");
      var hasCard = (card != undefined && card != null)
      var cardCost = card ? parseFloat(card.getAttribute("data-value")) : 0;
      var totalPrice = hourCost + cardCost;

      hourPriceOutput.innerHTML = toDisplayCost(hourCost, "AUD");
      cardPriceOutput.innerHTML = toDisplayCost(cardCost, "AUD");
      totalCostOutput.innerHTML = hasCard ? toDisplayCost(totalPrice, "AUD") : "Please select a card";
      totalCostOutput.setAttribute("data-value", totalPrice);
      if(!hasCard) { checkoutButton.setAttribute("disabled", true); }
      else{ checkoutButton.removeAttribute("disabled"); }

    }

    var cards = Array.from(document.querySelectorAll(".card"));

    cards.map(card => card.querySelector(".select-card-button").addEventListener("click", e => {
      var selectionClass = "selected";
      cards
        .filter(x => x.classList.contains(selectionClass))
        .map(x => x.classList.remove(selectionClass));
      card.classList.add(selectionClass);
      document.querySelector("#pickCard ul").classList.add("has-value");
      updateCostOutput();
    }))

    hourInput.addEventListener("keyup", updateCostOutput);
    hourInput.addEventListener("change", updateCostOutput);
    updateCostOutput();
  }

  function setupCheckout(token, checkoutUrl, giftPurchaseUrl){
    
    let handler = StripeCheckout.configure({
      key: token,
      image: './media/img/favicon.png',
      locale: 'auto',
      token: function(token) {
        return showLoader().then(finishWithToken(token, giftPurchaseUrl));
      }
    });

    var form = document.getElementById("giftForm");
    form.setAttribute("action", checkoutUrl);
    form.querySelector("input[type='submit']").classList.add("show");

    form.addEventListener('submit', function(e) {
      e.stopPropagation();
      e.preventDefault();

      if(document.querySelector("#numberOfHour").value < 3){
        alert("Please add a minimum of 3 hours for your gift card.");
        return;
      }
      
      handler.open({
        "name": 'mtime',
        "description": 'Join mtime',
        "email": form.querySelector("input[type='email'][name='payment-email']").value,
        "amount": parseInt(document.querySelector("#totalPrice").getAttribute("data-value")),
        "currency": "aud",
        "panelLabel": "Pay {{amount}} for gift card"
      });
    });
  }

  function finishWithToken(token, giftPurchaseUrl){
    var formData = Array.from(document.querySelectorAll("#giftForm input:not([type='submit'])"))
      .reduce((state, next) => {
        var names = next.getAttribute("name").split("-");
        if(state[names[0]] == null || state[names[0]] == undefined){
          state[names[0]] = {};
        }
        state[names[0]][names[1]] = next.value;
        return state;
      }, {});
    
    formData['items'] = [
      {
        "type": "https://schema.mtime.com.au/product/gift/hour-rate",
        "amount": parseInt(document.querySelector("input[name='hour']").value) || 0,
      },
      {
        "type": document.querySelector(".card.selected").getAttribute("data-schema"),
        "amount": 1
      },
    ];
    formData['description'] = document.querySelector("#purchaseDescription").innerHTML;
    formData['message'] = document.querySelector('textarea').value;
    formData['payment']['token'] = token['id'];
    
    return post(giftPurchaseUrl, formData)
      .then(_ => document.location.href="gift-thank.html")
      .catch(_ => alert("Unable to complete gift card purchase", "Please try again later"))
      .then(hideLoader);
  }

  
  function post(url, data) {
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.status == 200) { resolve(xmlhttp.response); }
        else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
      };
      xmlhttp.open("post", url, true);
      xmlhttp.setRequestHeader("Content-Type", "application/json");
      xmlhttp.send(JSON.stringify(data));
    });
  }
  
  function get(url) {
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (xmlhttp.status == 200) {
          setTimeout(function () { return resolve(JSON.parse(xmlhttp.response)) }, 10);
        }
        else if (xmlhttp.status != 0) { reject(xmlhttp.response); }
      };
      xmlhttp.open("get", url, true);
      xmlhttp.send();
    });
  }

  function showLoader(){
    return new Promise((resolve, reject) => {
      var loader = document.querySelector(".loader");
      loader.classList.add("show");
      function handleTransitionEnd(event){
          loader.removeEventListener("transitionend", handleTransitionEnd);
          resolve();
      }
      window.setTimeout(() => {
        loader.addEventListener("transitionend", handleTransitionEnd)
        loader.classList.add("visible");
      }, 10);
    });
    
  }

  function hideLoader(){
    return new Promise((resolve, reject) =>{
      function handleTransitionEnd(event){
        loader.removeEventListener("transitionend", handleTransitionEnd);
        loader.classList.remove("show");
        resolve();
      }
      var loader = document.querySelector(".loader");
      loader.addEventListener("transitionend", handleTransitionEnd);
      loader.classList.remove("visible");
    });
    
  }

})();
