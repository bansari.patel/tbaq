(function(){
  var configName = window.location.href.indexOf("https://www.mtime.com.au/")==0 ? "config.json" :  "local.config.json";

  get("media/js/"+configName).then(function(x){ setUpForm(x['bookWrittenAssessment']); });

  var requestedCode = document.location.search.replace("?code=","");

  if(requestedCode!=undefined && requestedCode!=null){
    var date = new Date(atob(requestedCode));
    document.querySelector("*[data-name='time']").innerHTML = date.getHours()+ ":" + date.getMinutes() + " " + date.toLocaleDateString();
    document.querySelector("section#startTest").classList.add("show");
    document.querySelector("section#bookingTest").classList.remove("show");
  }

  var minimumBookingTest = new Date();
  minimumBookingTest.setHours(minimumBookingTest.getHours()+3);
  minimumBookingTest.setSeconds(0);
  var input = document.querySelector("input[type='datetime-local']");
  input.min = minimumBookingTest.toISOString().replace("Z","").split(".")[0];
  var form = document.getElementById("bookForm");

  function setUpForm(url){
    form.addEventListener("submit", function(e){
      e.stopPropagation();
      e.preventDefault();
      console.log(url);
      var time = document.getElementById("startTime").value;
      var code = btoa(time);
      var email = document.getElementById("email").value;
      var link = document.location.origin + "/" + document.location.pathname + "?code="+code ;
      showLoader()
        .then(() => post(url, {code: code, email:  email, time: time, link: link}))
        .then(bookingCompleted)
        .catch(_ => alert("Unfortunately, we couldn't book your test at the moment. Please try again later."))
        .then(hideLoader);
    });
  }

  function bookingCompleted(){
    document.querySelector("section#bookingTest").classList.remove("show");
    document.querySelector("section#testBooked").classList.add("show");
  }

  function showLoader(){
    var loader = document.querySelector(".loader");
    return new Promise(function(resolve, reject){
      loader.classList.add("show");
      window.setTimeout(function(){
        loader.classList.add('visible');
        resolve();
      }, 50);
    });
  }
  function hideLoader(){
    var loader = document.querySelector(".loader");
    return new Promise(function(resolve, reject){
      loader.classList.remove("visible");
      window.setTimeout(function(){
        loader.classList.remove('show');
        resolve();
      }, 250);
    });
  }

  function post(url, data) {
    return new Promise(function(resolve, reject){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 200){ resolve(xmlhttp.response);}
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
      };
      xmlhttp.open("post", url, true);
      xmlhttp.setRequestHeader("Content-Type","application/json");
      xmlhttp.send(JSON.stringify(data));
    });
  }

  function get(url) {
    return new Promise(function(resolve, reject){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 200){ 
          setTimeout(function(){  return resolve(JSON.parse(xmlhttp.response)) }, 10);
        }
        else if(xmlhttp.status != 0){ reject(xmlhttp.response); }
      };
      xmlhttp.open("get", url, true);
      xmlhttp.send();
    });
  }

})();
